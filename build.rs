use std::process;

fn main() {
    if let Err(compile_error) = slint_build::compile("ui/main.slint") {
        eprintln!("Failed to compile main.slint: {compile_error}");
        process::exit(1);
    }
}
