use std::path::PathBuf;
use directories::UserDirs;
use log::error;
use nova_explorer::ui::side_controller::{Actions, Controller, PathType};
use nova_explorer::{init_logger, MainView, path_for_favorite_item};
use slint::{ComponentHandle, SharedString};

fn main() {
    init_logger();

    let user_dirs = UserDirs::new().expect("nova-explorer: cannot find a valid home directory!");
    let file_explorer_window = MainView::new().expect("nova-explorer: cannot start main window!");
    let controller = Controller::new(file_explorer_window.as_weak());
    let sender = controller.channel.clone();

    // Callback handlers
    file_explorer_window.on_cd_favorites(move |selector: SharedString| {
        if let Some(path) = path_for_favorite_item(selector.as_str(), &user_dirs) {
            let path = PathBuf::from(path);
            if let Err(error) = sender.send(Actions::ListFiles(path, PathType::Absolute)) {
                error!("Error sending ListFiles action to a controller {error}");
            }
        }
    });

    let sender = controller.channel.clone();
    file_explorer_window.on_change_subdir(move |dir_name: SharedString| {
        let sub_dir = PathBuf::from(dir_name.as_str());
        if let Err(error) = sender.send(Actions::ListFiles(sub_dir, PathType::Relative)) {
            error!("Error sending ListFiles action to a controller {error}");
        }
    });

    let sender = controller.channel.clone();
    file_explorer_window.on_go_up(move || {
        if let Err(error) = sender.send(Actions::GoUp) {
            error!("Error sending GoUp action to a controller {error}");
        }
    });

    controller.channel.send(Actions::ListFiles(PathBuf::from("/"), PathType::Absolute)).unwrap();

    file_explorer_window
        .run()
        .expect("nova-explorer: cannot start event loop!");
    controller.stop();
}
