use crate::{
    core::{list_dir, FilterOptions, Sorter, SortDirection, SortPredicate},
    generated_code::MainView
};
use log::error;
use slint::{ComponentHandle, Model, ModelRc, SharedString, VecModel, Weak};
use xdg_mime::SharedMimeInfo;
use std::{
    path::PathBuf,
    rc::Rc,
    sync::{mpsc::{self, Sender}, Arc, Mutex},
    thread::{self}
};

/// Describes the path handling during file listing operation.
pub enum PathType {
    /// The path is an absolue path.
    Absolute,
    /// The path is a relative one, it should be joined to the current working directory's path.
    Relative,
}

/// List of available actions that the Controller able to handle.
pub enum Actions {
    /// Goes up by one level in the directory tree.
    // Switches the `workind_dir` to its parent, (if it has), then lists its content.
    GoUp,
    ListFiles(PathBuf, PathType),
    Shutdown,
}

pub struct Controller {
    /// The communication channel between the controller and the worker thread.
    /// It allows sending actions such directory content listing, file operations.
    pub channel: Sender<Actions>,

    /// The current working directory.
    /// This field holds the path to the directory where file operations are performed.
    working_dir: Arc<Mutex<PathBuf>>,

    /// A weak reference to the main user interface.
    /// This allows the Controller to interact with the user interface without creating a strong reference cycle.
    ui: Weak<MainView>,
}

impl Controller {
    pub fn new(ui: Weak<MainView>) -> Self {
        let working_dir = Arc::new(Mutex::new(PathBuf::from("/")));
        let cwd = working_dir.clone();
        let (tx, rx) = mpsc::channel();
        let ui_handler = ui.clone();
        let mime_db = Arc::new(SharedMimeInfo::new());

        thread::spawn(move || loop {
            match rx.recv() {
                Ok(action) => match action {
                    Actions::GoUp => {
                        let mut cwd = cwd.lock().unwrap();
                        if cwd.pop() {
                            let parent = Arc::new(Mutex::new(cwd.clone()));
                            list_files(&cwd, &parent, &mime_db, &ui_handler);
                        }
                    }
                    Actions::ListFiles(path, PathType::Absolute) => list_files(&path, &cwd, &mime_db, &ui_handler),
                    Actions::ListFiles(sub_dir, PathType::Relative) => {
                        let new_path = cwd.lock().unwrap().join(sub_dir);
                        list_files(&new_path, &cwd, &mime_db, &ui_handler);
                    }
                    Actions::Shutdown => break,
                },
                Err(error) => error!("Error receiving an action: {error}"),
            }
        });

        Controller {
            channel: tx,
            working_dir,
            ui,
        }
    }

    /// Initiates the shutdown process of the worker thread
    /// by sending an action through a communication channel.
    /// Logs an error message if sending the shutdown action fails.
    ///
    /// # Note
    /// It is essential to ensure that the controller instance is properly shutdown by calling this method
    /// before the main() function returns.
    pub fn stop(&self) {
        if let Err(error) = self.channel.send(Actions::Shutdown) {
            error!("Cannot send shutdown action: {error}");
        }
    }
}

fn list_files(path: &PathBuf, working_dir: &Arc<Mutex<PathBuf>>, mime_db: &Arc<SharedMimeInfo>, ui: &Weak<MainView>) {
    let filter_options = FilterOptions::default();
    let path_clone = path.clone();
    
    match list_dir(path, &filter_options) {
        Ok(mut listed_files) => {
            update_working_dir(working_dir, path.clone());
            let mime_db = mime_db.clone();
            ui.upgrade_in_event_loop(move |handler| {
                sort_files(&handler, &mut listed_files);

                // convert a core::File into a slint::File
                let vec = VecModel::<crate::File>::default();
                for f in listed_files {
                    let slint_file = crate::File::from(f, &mime_db);
                    vec.push(slint_file);
                }

                // create a model for the view from the converted file list,
                // then set it to display in the ui
                let list_model_ = Rc::new(vec);
                let model_rc =
                    ModelRc::from(list_model_.clone() as Rc<dyn Model<Data = crate::File>>);
                handler
                    .global::<crate::FileListPanelAdapter>()
                    .set_files(model_rc);

                // change working dir text in the ui
                let path_str = path_clone.display().to_string();
                let working_dir = SharedString::from(path_str);
                handler.set_working_dir(working_dir);
            })
            .unwrap();
        }
        Err(error) => {
            let p = path.display();
            error!("Error listing files for {p}: {error}");
        }
    }
}

fn update_working_dir(current_dir: &Arc<Mutex<PathBuf>>, new_path: PathBuf) {
    *current_dir.lock().unwrap() = new_path;
}

fn sort_files(ui: &MainView, files: &mut [crate::core::File]) {
    let direction = SortDirection::from(ui.get_sort_direction());
    let predicate = SortPredicate::from(ui.get_sort_predicate());
    let sorter = Sorter::new(direction, predicate);
    sorter.sort(files)
}
