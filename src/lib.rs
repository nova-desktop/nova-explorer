use std::{path::Path, sync::Arc};
use linicon::lookup_icon;
use slint::{Image, SharedString};
use directories::UserDirs;
use redox_log::{OutputBuilder, RedoxLogger};

mod core;
pub mod ui;

#[allow(clippy::all)]
pub mod generated_code {
    slint::include_modules!();
}
pub use generated_code::*;
use xdg_mime::SharedMimeInfo;

impl generated_code::File {
    fn from(mut value: core::File, mime_db: &Arc<SharedMimeInfo>) -> Self {
        let ftype = match value.is_dir {
            true => generated_code::FileType::Directory,
            false => generated_code::FileType::Regular,
        };
        generated_code::File {
            name: SharedString::from(value.name()),
            icon: find_icon_for_file(&mut value, mime_db).unwrap_or_default(),
            ftype
        }
    }
}

pub fn path_for_favorite_item<'dirs>(selector: &str, user_dirs: &'dirs UserDirs) -> Option<&'dirs Path> {
    match selector {
        "home" => Some(user_dirs.home_dir()),
        "desktop" => user_dirs.desktop_dir(),
        "docs" => user_dirs.document_dir(),
        _ => Some(user_dirs.home_dir()),
    }
}

/// Finds and loads an icon for the given file based on its type or mime information.
///
/// It attempts to find an appropriate
/// icon for the file based on its type or mime information. If the file is a
/// directory, it returns the icon associated with the "inode-directory" type.
/// For regular files, it guesses the file's mime type, queries the mime database
/// for an icon name, and finally loads the corresponding icon.
///
/// # Arguments
///
/// * `file` - A mutable reference to the `core::File` for which to find an icon.
/// * `mime_db` - A reference to a shared mime database (`SharedMimeInfo`) containing
///   mime information used for determining file types and associated icons.
///
/// # Returns
///
/// An `Option<Image>` representing the loaded icon if found, or `None` if no
/// suitable icon is found or if there are errors in the icon loading process.
pub fn find_icon_for_file(file: &mut core::File, mime_db: &Arc<SharedMimeInfo>) -> Option<Image> {
    if file.is_dir {
        let folder_res = lookup_icon("inode-directory").next();
        if let Some(folder_icon) = folder_res.and_then(Result::ok) {
            return Image::load_from_path(&folder_icon.path).ok();
        }
    } else if let Some(mime_type) = mime_db.get_mime_types_from_file_name(&file.name).first() {
        if let Some(icon_name) = mime_db.lookup_generic_icon_name(mime_type) {
            let icon_res = lookup_icon(icon_name).next();
            if let Some(icon) = icon_res.and_then(Result::ok) {
                return Image::load_from_path(&icon.path).ok();
            }
        }
    }
    None
}

pub fn init_logger() {
    if let Err(error) = RedoxLogger::new()
        .with_output(
            OutputBuilder::stdout()
                .with_filter(log::LevelFilter::Error)
                .with_ansi_escape_codes()
                .build(),
        )
        .with_process_name("nova-explorer".into())
        .enable()
    {
        eprint!("WARNING: Cannot initalize logger: {error}")
    }
}
