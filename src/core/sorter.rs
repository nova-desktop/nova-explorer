use std::cmp::Ordering;

use super::File;

/// Specifies the order of the items in the UI's model.
/// Default is `SortDirection`.
#[derive(Copy, Clone, Debug, Default, PartialEq)]
pub enum SortDirection {
    // Values are arranged from the lowest to the highest.
    /// The lowest value will be placed on the top of the
    /// list. Sorting will continue and place the next increasing
    /// value in the list until it reaches the highest value that is
    /// placed on the bottom of the list.
    #[default]
    Ascending,

    /// Values are arranged from the highest to the lowest.
    /// The highest value will be placed on the top of the
    /// list. Sorting will continue and place the next decreasing
    /// value in the list until it reaches the lowest value that is
    /// placed on the bottom of the list.
    Descending,
}

impl From<crate::SortDirection> for SortDirection {
    fn from(value: crate::SortDirection) -> Self {
        match value {
            crate::SortDirection::Asc => Self::Ascending,
            crate::SortDirection::Desc => Self::Descending,
        }
    }
}

/// Represents the sorting criteria of items in a collection.
///
/// The `SortPredicate` enum defines different sorting options that can be used
/// to order items based on specific criteria. Each variant represents a distinct
/// sorting criterion.
#[derive(Copy, Clone, Debug, Default, PartialEq)]
pub enum SortPredicate {
    /// Sort items by their names
    #[default]
    Name,

    /// Sort items by their last modification timestamp
    LastModified,

    /// Sort items by their filesize
    Size,
}

impl From<crate::SortPredicate> for SortPredicate {
    fn from(value: crate::SortPredicate) -> Self {
        match value {
            crate::SortPredicate::Name => Self::Name,
            crate::SortPredicate::LastModified => Self::LastModified,
            crate::SortPredicate::Size => Self::Size,
        }
    }
}

pub trait Sort {
    fn sort(&self, files: &mut [File]);
}

/// Sorts items using a specified sorting algorithm based on the sorting `direction` and `predicate`.
pub struct Sorter {
    direction: SortDirection,
    predicate: SortPredicate
}

impl Sorter {
    /// Creates a new `Sorter` instance with the sorting direction, and predicate.
    pub fn new(direction: SortDirection, predicate: SortPredicate) -> Self {
        Self {direction, predicate }
    }

    /// Sorts the files based on the current `direction` and `predicate`.
    pub fn sort(&self, files: &mut [File]) {
        match self.direction {
            SortDirection::Ascending => {
                match self.predicate {
                    SortPredicate::Name => NameSorterAsc.sort(files),
                    SortPredicate::LastModified => LastModifiedSorterAsc.sort(files),
                    SortPredicate::Size => SizeSorterAsc.sort(files),
                }
            }
            SortDirection::Descending => {
                match self.predicate {
                    SortPredicate::Name => NameSorterDesc.sort(files),
                    SortPredicate::LastModified => LastModifiedSorterDesc.sort(files),
                    SortPredicate::Size => SizeSorterDesc.sort(files),
                }
            }
        }
    }
}

/// Sorts the files in ascending order by filename.
/// Directories comes first then files.
/// This sorter is case-sensitive.
pub struct NameSorterAsc;

impl Sort for NameSorterAsc {
    fn sort(&self, files: &mut [File]) {
        files.sort_by(|a, b| {
            if a.is_dir && b.is_dir {
                a.name.cmp(&b.name)
            } else if a.is_dir && !b.is_dir {
                Ordering::Less
            } else if !a.is_dir && b.is_dir {
                Ordering::Greater
            } else {
                a.name.cmp(&b.name)
            }
        })
    }
}

/// Sorts the files in descending order by name.
/// Directories comes first then files.
/// This sorter is case-sensitive.
pub struct NameSorterDesc;

impl Sort for NameSorterDesc {
    fn sort(&self, files: &mut [File]) {
        files.sort_by(|a, b| {
            if a.is_dir && b.is_dir {
                b.name.cmp(&a.name)
            } else if a.is_dir && !b.is_dir {
                Ordering::Less
            } else if !a.is_dir && b.is_dir {
                Ordering::Greater
            } else {
                b.name.cmp(&a.name)
            }
        })
    }
}

/// Sorts the files in ascending order by size.
/// Directories comes first then files.
pub(crate) struct SizeSorterAsc;

impl Sort for SizeSorterAsc {
    fn sort(&self, files: &mut [File]) {
        files.sort_by(|a, b| {
            if a.is_dir && b.is_dir {
                b.size.cmp(&a.size)
            } else if a.is_dir && !b.is_dir {
                Ordering::Less
            } else if !a.is_dir && b.is_dir {
                Ordering::Greater
            } else {
                a.size.cmp(&b.size)
            }
        })
    }
}

/// Sorts the files in descending order by size.
/// Directories comes first then files.
pub(crate) struct SizeSorterDesc;

impl Sort for SizeSorterDesc {
    fn sort(&self, files: &mut [File]) {
        files.sort_by(|a, b| {
            if a.is_dir && b.is_dir {
                b.size.cmp(&a.size)
            } else if a.is_dir && !b.is_dir {
                Ordering::Less
            } else if !a.is_dir && b.is_dir {
                Ordering::Greater
            } else {
                b.size.cmp(&a.size)
            }
        })
    }
}

/// Sorts the files in ascending order by their last modified date.
/// Directories comes first then files.
pub(crate) struct LastModifiedSorterAsc;

impl Sort for LastModifiedSorterAsc {
    fn sort(&self, files: &mut [File]) {
        files.sort_by(|a, b| {
            if a.is_dir && b.is_dir {
                a.date.cmp(&b.date)
            } else if a.is_dir && !b.is_dir {
                Ordering::Less
            } else if !a.is_dir && b.is_dir {
                Ordering::Greater
            } else {
                a.date.cmp(&b.date)
            }
        })
    }
}

/// Sorts the files in descending order by their last modified date.
/// Directories comes first then files.
pub(crate) struct LastModifiedSorterDesc;

impl Sort for LastModifiedSorterDesc {
    fn sort(&self, files: &mut [File]) {
        files.sort_by(|a, b| {
            if a.is_dir && b.is_dir {
                b.date.cmp(&a.date)
            } else if a.is_dir && !b.is_dir {
                Ordering::Less
            } else if !a.is_dir && b.is_dir {
                Ordering::Greater
            } else {
                b.date.cmp(&a.date)
            }
        })
    }
}