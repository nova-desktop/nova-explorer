use std::{
    fs::{self, DirEntry},
    io::Error,
    path::Path,
};

use chrono::{DateTime, Local};
use log::warn;

mod sorter;
pub use sorter::*;

pub const NOT_FOUND_PLACEHOLDER: &str = "N/A";

#[derive(Debug)]
pub struct File {
    pub date: String,
    pub name: String,
    pub is_dir: bool,
    pub size: u64,
}

impl File {
    pub fn name(&self) -> &String {
        &self.name
    }
}

impl From<DirEntry> for File {
    fn from(entry: DirEntry) -> Self {
        let name = match entry.file_name().into_string() {
            Ok(fname) => fname,
            Err(error) => {
                warn!("cannot read filename: {:?}", error);
                NOT_FOUND_PLACEHOLDER.to_string()
            }
        };

        let mut is_dir = true;
        let mut size = 0;
        let (is_dir, size, date) = match entry.metadata() {
            Ok(metadata) => {
                if !metadata.is_dir() {
                    is_dir = false;
                    size = metadata.len();
                }

                let date = match metadata.modified() {
                    Ok(modified) => {
                        let datetime_local: DateTime<Local> = modified.into();
                        let dt_formatted = datetime_local.format("%Y.%m.%d %H:%M");
                        dt_formatted.to_string()
                    }
                    Err(error) => {
                        warn!("cannot read last modification date for {name} : {error}",);
                        NOT_FOUND_PLACEHOLDER.to_string()
                    }
                };

                (is_dir, size, date)
            }
            Err(error) => {
                warn!("cannot read metadata for file {name}: {error}");
                (is_dir, 0, String::new())
            }
        };

        File {
            name,
            is_dir,
            size,
            date,
        }
    }
}

#[derive(Default)]
pub struct FilterOptions {
    pub show_hidden_files: bool,
}

/// Collects list of files and directories with their metadata in the given `path`
/// into a vector.
/// I/O errors related to the `path` are propagated up to the caller.
/// I/O errors related to filename and last modification date will not interrupt listing,
/// Instead, those values will be replaced with a placeholder indicating that the function failed to
/// read those values.
pub fn list_dir(dir: &Path, filter_options: &FilterOptions) -> Result<Vec<File>, Error> {
    let result: Vec<File> = fs::read_dir(dir)?
        .filter_map(|result| result.ok())
        .filter(|entry| {
            filter_options.show_hidden_files
                || !entry.file_name().to_string_lossy().starts_with('.')
        })
        .map(File::from)
        .collect();

    Ok(result)
}
